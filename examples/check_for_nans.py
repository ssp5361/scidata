#!/usr/bin/env python

# Checks all the HDF5 data in the current directory for NaNs and Infs
# (Usefull to check the sanity of checkpoints)

import numpy as np
import scidata.carpet.hdf5 as h5
import sys

if(len(sys.argv) < 2):
    print("Usage %s [file1.h5 [file2.h5 [...]]]" % sys.argv[0])
    exit(0)

dset = h5.dataset(sys.argv[1:])

for name in dset.metadata:
    rdata = dset.get_dataset(*name)
    var   = np.array(rdata)
    nnans = np.logical_not(np.isfinite(var)).sum()
    if nnans > 0:
        print("%s NaNs found in %s" % (repr(nnans), rdata.name))

