#!/usr/bin/env python
#
# Plots a scalar field (stored in a CarpetHDF5 file) on a plane

# =============================================================================
# Initialization
# =============================================================================
import matplotlib
matplotlib.use("Agg")

import matplotlib.pyplot as plt
import numpy as np
from numpy import inf
import scidata.carpet.hdf5 as h5
import scidata.carpet.grid as carpet
import sys
import optparse
import os

from scidata.utils import locate

usage = "%prog [options] field"
parser = optparse.OptionParser(usage)
parser.add_option("-c", "--contour", dest="contour", action="store_true",\
        help="Show the contour levels")
parser.add_option("--cmap", dest="cmap", help="Color map")
parser.add_option("-g", "--reflevel", dest="reflevel", help=\
        "Which refinement level to plot")
parser.add_option("-l", "--log", dest="log", action="store_true",\
        help="Compute log of the data")
parser.add_option("-p", "--plane", dest="plane", help=\
        "Which plane to output [defaults to xz]")
parser.add_option("--range", dest="crange", help=\
        "Provide a custom range in the form [vmin, vmax] for plotting")
parser.add_option("-r", "--rescale", dest="rescale", action="store_true",\
        help="Use a different (adapted) scale for each frame")
parser.add_option("-s", "--subtract", dest="subtract", action="store_true", \
        help="Subtract the initial value of the field")
parser.add_option("--xmin", dest="xmin", help="Minimum x")
parser.add_option("--xmax", dest="xmax", help="Maximum x")
parser.add_option("--ymin", dest="ymin", help="Minimum y")
parser.add_option("--ymax", dest="ymax", help="Maximum y")

(options, args) = parser.parse_args()

if len(args) != 1:
    parser.error("No field specified")

field = args[0]

if options.log is None:
    options.log = False

if options.plane is None:
    options.plane = "xz"

if options.crange is not None:
    options.crange = eval(options.crange)

if options.reflevel is None:
    options.reflevel = 0
else:
    options.reflevel = int(options.reflevel)

if options.rescale is None:
    options.rescale = False

if options.subtract is None:
    options.subtract = False

if options.xmin is None:
    options.xmin = -inf
else:
    options.xmin = float(options.xmin)

if options.xmax is None:
    options.xmax = inf
else:
    options.xmax = float(options.xmax)

if options.ymin is None:
    options.ymin = -inf
else:
    options.ymin = float(options.ymin)

if options.ymax is None:
    options.ymax = inf
else:
    options.ymax = float(options.ymax)

filename  = field + "." + options.plane + ".h5"

# =============================================================================
# Config
# =============================================================================
# Output format
format = "png"
# Number of levels for the colormap
nlevels = 20
# Output directory
outdir    = field + ".r" + str(options.reflevel) + "." + options.plane +\
    ".output"
# Output filename prefix (the output will be prefix.iteration.format)
prefix    = field + ".r" + str(options.reflevel) + "." + options.plane

if not os.path.isdir(outdir):
    os.mkdir(outdir)

datafile  = h5.dataset(locate(filename))
grid = datafile.get_reflevel(reflevel=options.reflevel,
        iteration=datafile.iterations[0])
field_init = datafile.get_reflevel_data(grid,
        iteration=datafile.iterations[0])

if not options.rescale and options.crange is None:
    print("Generating global field density levels..."),
    field_min = []
    field_max = []
    for iter in datafile.iterations:
        grid = datafile.get_reflevel(iteration=iter, reflevel=options.reflevel)
        field = datafile.get_reflevel_data(grid, iteration=iter)
        if options.subtract:
            field = field - field_init
        if options.log:
            field = np.log10(np.abs(field) + sys.float_info.epsilon)
        field_min.append(field.min())
        field_max.append(field.max())
    field_min = min(field_min)
    field_max = max(field_max)
    step = (field_max - field_min) / nlevels
    levels = np.arange(field_min, field_max, step)
    print("done!")

if options.crange is not None:
    field_min = options.crange[0]
    field_max = options.crange[1]
    step = (field_max - field_min) / nlevels
    levels = np.arange(field_min, field_max, step)

iterfill  = len(str(datafile.iterations[-1]))
framefill = len(str(len(datafile.iterations)))
for k in range(len(datafile.iterations)):
    iter = datafile.iterations[k]
    print("Processing frame " + str(k+1).zfill(framefill) + "/" +\
        str(len(datafile.iterations)) + "..."),
    grid  = datafile.get_reflevel(iteration=iter, reflevel=options.reflevel)
    field = datafile.get_reflevel_data(grid, iteration=iter)
    if options.subtract:
        field = field - field_init
    if options.log:
        field = np.log10(np.abs(field) + sys.float_info.epsilon)

    if options.rescale:
        field_min = field.min()
        field_max = field.max()
        step = (field_max - field_min) / nlevels
        levels = np.arange(field_min, field_max, step)

    if options.contour:
        x, y, = grid.mesh()
        im = plt.contourf(x, y, field, levels)
    else:
        dw = grid.origin
        up = grid.upperbound()
        im = plt.imshow(field.transpose(), origin='lower',
                cmap=options.cmap, vmin=field_min, vmax=field_max,
                extent=(dw[0], up[0], dw[1], up[1]), aspect='auto')

    plt.colorbar(im)

    dset  = datafile.get_dataset(iteration=iter)
    plt.title('time = %10.2f' % dset.attrs['time'])

    bound = list(plt.axis())
    bound[0] = max(bound[0], options.xmin)
    bound[1] = min(bound[1], options.xmax)
    bound[2] = max(bound[2], options.ymin)
    bound[3] = min(bound[3], options.ymax)
    plt.axis(bound)

    plt.savefig(outdir + "/" + prefix + "." +\
        str(iter).zfill(iterfill) + ".png")
    plt.close()

    print("done!")
