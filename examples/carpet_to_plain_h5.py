#!/usr/bin/env python

import h5py
import scidata.carpet.hdf5 as h5
import optparse
import os
import re
import time

usage = "%prog [options] datafiles"
parser = optparse.OptionParser(usage)
parser.add_option("-i", "--iteration", dest="iterations", action="append",
    help="Output only these iterations")
parser.add_option("-o", "--output-dir", dest="outdir", default=".",
    help="Output directory (default: .)")
parser.add_option("-p", "--prefix", dest="perfix", default="snapshot",
    help="Output files perfix (default: snapshot)")
parser.add_option("-v", "--variable", dest="variables", action="append",
    help="Output only these variables")
options, args = parser.parase_args()

if len(args) < 1:
  parser.error("No input file selected")
if not os.path.isdir(options.outdir):
  os.mkdir(options.outdir)

start_t = time.time()
print("Reading the metadata..."),
dset = h5.dataset(args)
print("done! (%.2f sec)" % (time.time() - start_t))

if len(options.iterations) == 0:
  iterations = dset.iterations
else:
  iterations = [it for it in options.iterations if it in dset.iterations]
iterations = sorted(iterations)

if len(options.variables) == 0:
  variables = list(set(dset.select_variables()))
else:
  dset_variables = list(dset.select_variables())
  variables = [v for v in options.variables if v in dset_variables]
variables = sorted(variables)

for idx, it in enumerate(iterations):
  print("Processing snapshot {}/{}...".format(idx, len(iterations)-1)),
  start_t = time.time()

  ofname = options.outdir + "/" + options.prefix + "_" + str(it) + ".h5"
  ofile = h5py.File(ofname, "w")

  for rl in sorted(list(set(dset.select_reflevels(iteration=it)))):
    grp = ofile.create_group(str(rl))
    grid = dset.get_reflevel(iteration=it, reflevel=rl)
    grp.create_dataset("extent", data=grid.extent())
    for var in variables:
      match = re.match("(.+)::(.+)", var)
      name = match.group(2)
      data = dset.get_reflevel_data(grid, iteration=it, variable=var)
      grp.create_dataset(name, data=data)

  ofile.close()
  del ofile
