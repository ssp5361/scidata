#!/usr/bin/env python

import csv
import scidata.simfactory
import scidata.utils
import sys
import optparse

usage = "%prog [options] simulations"
parser = optparse.OptionParser(usage)
parser.add_option("-c", "--csv", action="store_true", dest="csv",\
        help="Output in CSV format", default=False)
parser.add_option("-d", "--diff", action="store_true", dest="diff",\
        help="Output the difference between the simulations", default=False)
parser.add_option("-p", "--parameter", action="append", dest="opts",\
        help="Include the value of the given 'thorn::option' in the output")
parser.add_option("-t", "--thornlist", action="store_true", dest="thornlist",\
        help="Print the thornslits")

(options, args) = parser.parse_args()

if len(args) < 1:
    parser.error("No input file selected")

db = scidata.simfactory.database(args)

opts = []
if options.opts is not None:
    for o in options.opts:
        opts.append(tuple(o.split('::')))

table = db.print_sims()

if options.thornlist:
    table += db.print_thornlist()

if options.diff:
    table += db.print_diffs()

table += db.print_options(opts)

if options.csv:
    csv_writer = csv.writer(sys.stdout, dialect='excel')
    csv_writer.writerows(table)
else:
    print(scidata.utils.print_table(table))
