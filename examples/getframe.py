#!/usr/bin/env python

import scidata.carpet.ascii
import scidata.carpet.hdf5
import scidata.monodataset
import scidata.utils
import scidata.xgraph
import optparse
import re

def parse(filename, reflevel):
    fre = re.match(r"(.+)\.?(\w)?\.(\w+)", filename)
    if fre is None:
        raise scidata.utils.FileTypeError(filename)
    ext  = fre.group(3)

    if ext == "h5":
        data = scidata.carpet.hdf5.parse_1D_file(filename, reflevel)
    elif ext == "asc":
        data = scidata.carpet.ascii.parse_1D_file(filename, reflevel)
    elif ext == "xg" or ext == "yg":
        data = scidata.xgraph.parsefile(filename)
    else:
        raise scidata.utils.FileTypeError(filename)

    return data

usage = "%prog -o output [options] datafile"
parser = optparse.OptionParser(usage)
parser.add_option("-o", "--output", dest="output", help="Output file")
parser.add_option("-r", "--reflevel", dest="reflevel", help="Refinement level")
parser.add_option("-t", "--time", dest="time", help="Time to extract")

(options, args) = parser.parse_args()

if len(args) < 1:
    parser.error("No input file selected")

if options.output is None:
    parser.error("No output file selected")

data = parse(args[0], options.reflevel)

if options.time is None:
    options.time = data.time[-1]

frame = data.find_frame(float(options.time))
frame.sort()
frame.write(options.output)
