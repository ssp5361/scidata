#!/usr/bin/env python

import copy
import numpy
import scidata.carpet.ascii
import scidata.carpet.hdf5
import scidata.pygraph
import scidata.monodataset
import scidata.utils
import scidata.xgraph
import optparse
import re

def readfile(filename, reflevel):
    fre = re.match(r"(.+)\.?(\w)?\.(\w+)", filename)
    if fre is None:
        raise scidata.utils.FileTypeError(filename)
    ext  = fre.group(3)

    if ext == "h5":
        try:
            data = scidata.pygraph.parsefile(filename)
        except scidata.utils.FileTypeError:
            data = scidata.carpet.hdf5.parse_1D_file(filename, reflevel)
    elif ext == "asc":
        data = scidata.carpet.ascii.parse_1D_file(filename, reflevel)
    elif ext == "xg" or ext == "yg":
        data = scidata.xgraph.parsefile(filename)
    else:
        raise scidata.utils.FileTypeError(filename)

    return data

def readfiles(filenames, reflevel):
    if type(filenames) is str:
        return readfile(filenames, reflevel)
    else:
        data = readfile(filenames[0], reflevel)
        data.merge([readfile(f, reflevel) for f in filenames[1:]])
        return data

usage = "%prog -o output [options] datafiles"
parser = optparse.OptionParser(usage)
parser.add_option("-o", "--output", dest="output", help="Output file")
parser.add_option("-r", "--reflevel", dest="reflevel", help="Refinement level")
parser.add_option("-d", "--std-dev", dest="stddev", action="store_true",
        help="Compute also the standard deviation")
parser.add_option("-s", "--start", dest="start", help="Initial time")
parser.add_option("-e", "--end", dest="end", help="Final time")

(options, args) = parser.parse_args()

if len(args) < 1:
    parser.error("No input file selected")

if options.output is None:
    parser.error("No output file selected")

data = readfiles(args, options.reflevel)

if options.start is None:
    options.start = data.time[0]
else:
    options.start = float(options.start)
if options.end is None:
    options.end = data.time[-1]
else:
    options.end = float(options.end)

timestep = data.time[1] - data.time[0]

idx = 1
average = data.find_frame(options.start)
average.metadata = {}

ctime = options.start
while ctime < options.end:
    idx += 1
    ctime += timestep
    average += data.find_frame(ctime)
average.data_y = average.data_y / idx

if options.stddev:
    frame = data.find_frame(options.start)
    stddev = (frame - average)*(frame - average)
    del frame

    ctime = options.start
    while ctime < options.end:
        ctime  += timestep
        frame   = data.find_frame(ctime)
        stddev += (frame - average)*(frame - average)
    stddev.data_y = numpy.sqrt(stddev.data_y/idx)

    assert(stddev.data_y.shape[0] == average.data_x.shape[0])
    ofile = open(options.output, "w")
    ofile.write("# 1:x 2:avg 3:stddev\n")
    for i in xrange(stddev.data_y.shape[0]):
        ofile.write("%.19g %.19g %.19g\n" % (average.data_x[i],
            average.data_y[i], stddev.data_y[i]))
    ofile.close()
else:
    ofile = open(options.output, "w")
    ofile.write("# 1:x 2:avg\n")
    for i in xrange(average.data_y.shape[0]):
        ofile.write("%.19g %.19g\n" % (average.data_x[i],
            average.data_y[i]))
    ofile.close()
