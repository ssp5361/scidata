Scientific data library
=======================

Utilities to import scientific data from various datafile formats.
Currently the library supports mainly [Cactus](http://cactuscode.org/)/
[Carpet](http://carpetcode.org/).

Dependencies
------------

Requirements

* python-2.7, python-3.x is still not supported
* [h5py](https://www.h5py.org/)
* [numpy](http://www.numpy.org/)
* [scipy](https://www.scipy.org/)

Optional

* [matplotlib](https://matplotlib.org/)
* [texttable](https://github.com/foutaise/texttable)

Note that the [python anaconda distribution](https://www.anaconda.com/download)
already includes all dependencies needed by scidata. Installing anaconda is the
recommended way to setup a python stack for scidata.

Installation
------------

After all the required packages are installed, scidata can be installed using
the following commands

~~~
    $ git clone https://dradice@bitbucket.org/dradice/scidata.git
    $ cd scidata
~~~

To install scidata for all users (this requires administrative privileges) run

~~~
    $ python setup.py install
~~~

Otherwise scidata can be installed in the $HOME directory using the command

~~~
    $ python setup.py install --user
~~~

If scidata is installed in the `$HOME` directory, then it is necessary to add
`$HOME/Library/Python/2.7/lib/python/site-packages` (MacOS), or
`$HOME/.local/lib/python2.7/site-packages` (Linux) to the `PYTHONPATH` environment
variable. For example, when running bash on Linux it is necessary to add

~~~
    export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python2.7/site-packages
~~~

to the `.bashrc` file to enable scidata.

The `setup.py` script also installs example scripts and utilities. If scidata is
installed in the `$HOME` directory, then it is  necessary to add
`$HOME/Library/Python/2.7/bin` (MacOS), or `$HOME/.local/bin` (Linux) to the system
PATH to be able to run these scripts. For example, when running bash on Linux
it is necessary to add

~~~
    export PATH=$PATH:$HOME/.local/bin
~~~

to the `.bashrc` file to enable the scidata executables.
