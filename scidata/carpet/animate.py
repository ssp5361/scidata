from matplotlib import animation
from matplotlib.colors import LogNorm, Normalize
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import numpy as np
import scidata.carpet.hdf5 as h5

class ScalarField(object):
    """
    Class representing a scalar field
    """
    def __init__(self, fieldname):
        """
        fieldname : name of a Cactus grid function
        """
        self.fieldnames = [fieldname]

    def get_fieldnames(self):
        """
        Get list of Cactus grid function to read
        """
        return self.fieldnames

    def get_reflevel_data(self, dset, reflevel, iteration=None):
        """
        Get value of the scalar field

        * dset      : h5.dataset object
        * reflevel  : carpet.level object
        * iteration : iteration to read
        """
        return self.compute(
            [dset.get_reflevel_data(reflevel, iteration=iteration, variable=vn)
                for vn in self.fieldnames]
        )

    def compute(self, fields):
        """
        Compute the scalar field given the input grid functions
        """
        return fields[0]

class AnimationMaker(object):
    """
    Class for handling 2D animations

    TODO: this class does not draw correctly disconnected level structures.
    Instead the convex hull of each level is plotted.
    """
    def __init__(self, dset, field,
            extent=[None, None, None, None],
            clabel=None,
            levels=None,
            norm='linear',
            plot_rlevels=False,
            tlabel=r'$t = {:.2f}$',
            ulength=1,
            utime=1,
            verbose=False,
            xlabel=r'$x$',
            ylabel=r'$y$',
            **kwargs):
        """
        Setup a new animation

        * dset         : must be an h5.dataset object
        * field        : must be the name of a field in the h5.dataset or an
                         object of class ScalarField

        * clabel       : label for the colorbar
        * extent       : [xmin, xmax, ymin, ymax]
        * levels       : levels to plot (if None everything is plotted)
        * norm         : matplotlib.colors.Normalize object
                         special values:
                         . 'linear' --> Normalize(min, max)
                         . 'log'    --> LogNorm(min, max)
        * plot_rlevels : if True also plots the grid structure
        * tlabel       : string used for the time label
        * ulength      : length unit
        * utime        : time unit
        * verbose      : print progress
        * xlabel       : x-label
        * ylabel       : y-label

        * kwargs       : will be passed to imshow
        """
        self.dset = dset
        if type(field) == str:
            self.field = ScalarField(field)
        elif isinstance(field, ScalarField):
            self.field = field
        else:
            raise TypeError("field should be a string or a ScalarField")

        self.levels = levels
        if self.levels is None:
            nlevels = []
            for it in dset.iterations:
                # Remember that we count from zero
                nlevels.append(max(dset.select_reflevels(iteration=it))+1)
            self.levels = range(max(nlevels))

        if type(norm) == str:
            vmin, vmax = self.calc_extrema()
            if norm == "linear":
                norm = Normalize(vmin, vmax)
            elif norm == "log":
                vmin = max(vmin, np.spacing(1.0))
                norm = LogNorm(vmin, vmax)
            else:
                raise ValueError("Unkown normalization type: " + self.norm)

        self.plot_rlevels = plot_rlevels

        self.tlabel = tlabel

        self.ulength = ulength
        self.utime = utime

        self.verbose = verbose

        self.fig, self.ax = plt.subplots()

        self.title = self.ax.set_title("")
        self.ax.set_xlabel(xlabel)
        self.ax.set_ylabel(ylabel)

        self.im = [self.ax.imshow(np.nan*np.ones((1,1)), origin='lower',
            norm=norm, **kwargs) for i in self.levels]
        self.cbar = plt.colorbar(self.im[0])
        if clabel is not None:
            self.cbar.set_label(clabel)

        self.rects = []
        if self.plot_rlevels:
            for il in self.levels:
                r = Rectangle((0,0), 0, 0, fill=False)
                r.set_visible(False)
                self.rects.append(r)
                self.ax.add_patch(r)

        if extent[0] is not None:
            self.ax.set_xlim(xmin=extent[0])
        if extent[1] is not None:
            self.ax.set_xlim(xmax=extent[1])
        if extent[2] is not None:
            self.ax.set_ylim(ymin=extent[2])
        if extent[3] is not None:
            self.ax.set_ylim(ymax=extent[3])

        # This is to prevent showing an empty plot
        plt.close()

    def animate(self, **kwargs):
        """
        Create an animation

        kwargs : will be passed to FuncAnimation
        """
        return animation.FuncAnimation(self.fig, self.__draw_plot__,
                frames=range(len(self.dset.iterations)),
                init_func=self.__init_plot__, **kwargs)

    def calc_extrema(self, ilevel=None):
        """
        Compute the extrema of a function

        * ilevel : which refinement level to use
        """
        if ilevel is None:
            ilevel = self.levels[0]
        vmin, vmax = np.inf, -np.inf
        for it in self.dset.iterations:
            grid = self.dset.get_reflevel(iteration=it, reflevel=ilevel)
            if grid is None:
                continue
            data = self.field.get_reflevel_data(self.dset, grid, iteration=it)
            vmin = min(vmin, data.min())
            vmax = max(vmax, data.max())
        return vmin, vmax

    def __init_plot__(self):
        """\
        Initialize the animation
        """
        self.title.set_text("")
        for il in self.levels:
            self.im[il].set_data(np.nan*np.ones((1,1)))
            if self.plot_rlevels:
                self.rects[il].set_visible(False)

        # Return all artists to matplotlib
        return self.im + self.rects + [self.title]

    def __draw_plot__(self, idx):
        """\
        Create a new frame

        * it : iteration number
        """
        if self.verbose:
            print("Visualizing frame {}/{}...".format(
                idx, len(self.dset.iterations)-1))

        it = self.dset.iterations[idx]
        self.grid = self.dset.get_grid(iteration=it)
        self.grid.scale(self.ulength)
        self.title.set_text(self.tlabel.format(self.dset.get_time(it)*self.utime))
        for ig, rlevel in enumerate(self.grid):
            data = self.field.get_reflevel_data(self.dset, rlevel, iteration=it)

            self.im[ig].set_data(data.transpose())
            self.im[ig].set_extent(rlevel.extent())

            if self.plot_rlevels:
                self.rects[ig].set_x(rlevel.origin[0])
                self.rects[ig].set_y(rlevel.origin[1])
                self.rects[ig].set_width(rlevel.upperbound()[0] -
                        rlevel.origin[0])
                self.rects[ig].set_height(rlevel.upperbound()[1] -
                        rlevel.origin[1])
                self.rects[ig].set_visible(True)

        # Return all the updated matplotlib artists
        return self.im + self.rects + [self.title]
