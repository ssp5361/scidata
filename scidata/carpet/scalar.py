import numpy
import re

def readfile(filename):
    numbre = r"-?\d+\.*\d*e?-?\d*"
    linere = r"(" + numbre + r")\s(" + numbre + r")\s(" + numbre + r")"

    L = []

    for l in open(filename):
        dataline = re.match(linere, l)
        if dataline is not None:
            iter = float(dataline.group(1))
            time = float(dataline.group(2))
            data = float(dataline.group(3))
            L.append([iter, time, data])

    return numpy.array(L)
