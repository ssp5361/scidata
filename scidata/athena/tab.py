"""
Athena++ ASCII output
"""

import numpy as np
import os
import re

def parse_filename(fname):
    """
    Parse the filename of an Athena++ 1D ASCII table output

    * fname     : full path to the file to read

    Returns a tuple (problem_id, block_id, out_id)
    """
    bname = os.path.basename(fname)
    match = re.match("(.+).block(\d+).out(\d+).\d{5}.tab", bname)
    if match is None:
        raise ValueError("Could not parse file name \"{}\"".format(fname))
    return match.group(1), int(match.group(2)), int(match.group(3))

def read_file_metadata(fname):
    """
    Read the metedata of an Athena++ 1D ASCII table output

    * fname     : full path to the file to read

    Returns a tuple (time, cycle, gname, fields)

    * time      : time of the output [float]
    * cycle     : iteration number of the output [int]
    * gname     : variable group [str]
    * hasdim    : which dimensions are included in the output [bool]
    * fields    : list of fields in the output
    """
    fobj = open(fname, "r")

    pattern  = "# Athena\+\+ data at time=(\d\.\d+e[\+\-]\d+)  "
    pattern += "cycle=(\d+)  variables=(.+)$"
    match = re.match(pattern, fobj.readline())
    if match is None:
        raise ValueError(
            "Could not parse header of file: \"{}\"".format(fname))
    time = float(match.group(1))
    cycle = int(match.group(2))
    gname = match.group(3).strip()

    fields = fobj.readline().split()[1:]

    hasdim = [False, False, False]
    if "i" in fields:
        hasdim[0] = True
    if "j" in fields:
        hasdim[1] = True
    if "k" in fields:
        hasdim[2] = True

    fobj.close()

    return time, cycle, gname, hasdim, fields

class output_group:
    """
    This class stores the metadata associated with a signle Athena++ 1D ASCII
    output group. It is not intended to be used by the final user.

    Contents:
    * outid       : output ID [int]
    * gname       : variable group [str]
    * cycles      : a list of cycles
    * hasdim      : which dimensions are included in the output [bool]
    * ndim        : number of dimensions of the data [int]
    * variables   : a list of variables

    * cycle2files : a dictionary mapping cycles to file names {cyle: [fnames]}
    * cycle2time  : a dictionary mapping cycles to times {cycle: time}
    """
    def __init__(self, outid):
        self.outid = outid
        self.gname = None
        self.cycles = []
        self.hasdim = []
        self.variables = []
        self.cycle2files = {}
        self.cycle2time = {}

    def add_file(self, fname):
        """
        Adds a file to the collection
        """
        _, _, outid = parse_filename(fname)
        if outid != self.outid:
            raise ValueError("File \"{}\" ".format(fname) +
                    "appears to be part of a different output group")

        time, cycle, gname, hasdim, fields = read_file_metadata(fname)
        if self.gname is None:
            self.gname = gname
        elif gname != self.gname:
            raise ValueError("File \"{}\" ".format(fname) +
                    "appears to be part of a different output group")
        if not self.hasdim:
            self.hasdim = hasdim
            self.ndim = sum(self.hasdim)
        elif self.hasdim != hasdim:
            raise ValueError("File \"{}\" ".format(fname) +
                    "appears to be part of a different output group")
        if not self.variables:
            self.variables = fields[2*self.ndim:]
        elif self.variables != fields[2*self.ndim:]:
            raise ValueError("File \"{}\" ".format(fname) +
                    "appears to be part of a different output group")
        if cycle not in self.cycles:
            self.cycles.append(cycle)
            self.cycle2files[cycle] = [fname]
            self.cycle2time[cycle] = time
        else:
            self.cycle2files[cycle].append(fname)

    def get_time(self, cycle):
        """
        Get the time corresponding to a given cycle
        """
        return self.cycle2time[cycle]

    def get_var_index(self, variable=None):
        """
        Get the index of a variable
        """
        if variable is None:
            return 2*self.ndim
        elif variable in self.variables:
            return self.variables.index(variable) + 2*self.ndim
        else:
            raise ValueError("Variable \"{}\" not found!".format(variable))

    def read_var_at_cycle(self, variable=None, cycle=None):
        """
        Read a given variable at a given cycle number

        Returns a list of tuples [(blockid, coords, var)] with the data from
        each mesh block
        * blockid : block ID [int]
        * coords  : a tuple with the coordinates [(np.array,...)]
        * var     : the data in each block [np.array]
        """
        cid = self.get_var_index(variable)
        if cycle is None:
            cycle = self.cycles[0]

        out = []
        for fname in self.cycle2files[cycle]:
            _, bid, _ = parse_filename(fname)
            cols = [1]
            if self.ndim > 1:
                cols.append(3)
            if self.ndim > 2:
                cols.append(5)
            cols.append(cid)
            data = np.loadtxt(fname, usecols=cols, unpack=True)
            out.append((bid, data[:-1], data[-1]))

        return out

    def read_var_at_cycle_1d(self, variable=None, cycle=None, ip=None, jp=None, kp=None):
        """
        Get a variable along a particular direction at a particular time

        If the data is 1D, then ip, jp, kp are ignored. Otherwise, the output
        is made for fixed, not None, values of ip, jp, or kp. For 2D data one
        of the indices, associated with the dimension suppressed in the output
        is ignored, of the other two only one should be None. For 3D data only
        1 of ip, jp, and kp can be None.

        For example, for 3D output. If ip is None, and jp, and kp are ints,
        then this method returns:

            xi[i]  = coords[0][i]
            var[i] = variable[i, j=jp, k=kp]

        Returns a tuple (xi, var):
        * xi  : coordinate of the output
        * var : variable at xi
        """
        cid = self.get_var_index(variable)
        if cycle is None:
            cycle = self.cycles[0]

        if self.ndim == 1:
            return self.__read_cid_at_cycle_1d__(cid, cycle)
        elif self.ndim == 2:
            idx = []
            if self.hasdim[0]:
                idx.append(ip)
            if self.hasdim[1]:
                idx.append(jp)
            if self.hasdim[2]:
                idx.append(kp)
            idx = tuple(idx)
            if not (idx[0] is not None or idx[1] is not None):
                raise ValueError("Inconsistent slicing specification")
            if not (idx[0] is None or idx[1] is None):
                raise ValueError("Inconsistent slicing specification")
            return self.__read_cid_at_cycle_2d__(cid, cycle, *idx)
        elif self.ndim == 3:
            if not (sum([idx is None for idx in [ip,jp,kp]]) == 1):
                raise ValueError("Inconsistent slicing specification")
            return self.__read_cid_at_cycle_3d__(cid, cycle, ip, jp, kp)
        else:
            raise ValueError("Requested to read data, but no data files have been opened")

    def __read_cid_at_cycle_1d__(self, cid, cycle):
        x_glob, v_glob = [], []
        for fname in self.cycle2files[cycle]:
            xl, vl = np.loadtxt(fname, usecols=(1, cid), unpack=True)
            x_glob.append(xl)
            v_glob.append(vl)
        x = np.concatenate(x_glob)
        v = np.concatenate(v_glob)
        x, idx = np.unique(x, return_index=True)
        return x, v[idx]

    def __read_cid_at_cycle_2d__(self, cid, cycle, i0, i1):
        xi_glob, v_glob = [], []
        for fname in self.cycle2files[cycle]:
            with open(fname) as fobj:
                for dline in fobj:
                    if dline[0] == '#':
                        continue
                    data = dline.split()
                    i = int(data[0])
                    j = int(data[2])
                    if i0 is None:
                        if j != i1:
                            continue
                        xi_glob.append(float(data[1]))
                        v_glob.append(float(data[cid]))
                    else:
                        if i != i0:
                            continue
                        xi_glob.append(float(data[3]))
                        v_glob.append(float(data[cid]))
        x = np.array(xi_glob)
        v = np.array(v_glob)
        x, idx = np.unique(x, return_index=True)
        return x, v[idx]

    def __read_cid_at_cycle_3d__(self, cid, cycle, i0, i1, i2):
        xi_glob, v_glob = [], []
        for fname in self.cycle2files[cycle]:
            with open(fname) as fobj:
                for dline in fobj:
                    if dline[0] == '#':
                        continue
                    data = dline.split()
                    i = int(data[0])
                    j = int(data[2])
                    k = int(data[4])
                    if i0 is None:
                        if j != i1 or k != i2:
                            continue
                        else:
                            xi_glob.append(float(data[1]))
                            v_glob.append(float(data[cid]))
                    elif i1 is None:
                        if i != i0 or k != i2:
                            continue
                        else:
                            xi_glob.append(float(data[3]))
                            v_glob.append(float(data[cid]))
                    else:
                        if i != i0 or j != i1:
                            continue
                        else:
                            xi_glob.append(float(data[5]))
                            v_glob.append(float(data[cid]))
        x = np.array(xi_glob)
        v = np.array(v_glob)
        x, idx = np.unique(x, return_index=True)
        return x, v[idx]

    def sort(self):
        self.cycles = sorted(self.cycles)

class dataset:
    """
    A class representing the full Athena++ 1D ASCII table output

    * groups    : a list of output_groups

    * id2group  : a dictionary mapping output IDs to groups
    * gname2id  : a dictionary mapping output group names to IDs
    * id2vnames : a dictionary mapping output IDs to variable lists
    """
    def __init__(self, filenames):
        """
        Initialize the dataset given a file name or a list of file names

        * filenames : list of all files to read
        """
        self.id2group = {}
        for filename in filenames:
            _, _, outid = parse_filename(filename)
            if not self.id2group.has_key(outid):
                self.id2group[outid] = output_group(outid)
            self.id2group[outid].add_file(filename)

        self.groups = self.id2group.values()

        self.gname2id  = {}
        self.id2vnames = {}
        for group in self.groups:
            group.sort()
            self.gname2id[group.gname]  = group.outid
            self.id2vnames[group.outid] = group.variables

    def read_var_at_cycle(self, gname=None, variable=None, cycle=None):
        """
        Read a given variable from a given group at a given time
        """
        outid = self.__select_output_id__(gname, variable)
        return self.id2group[outid].read_var_at_cycle(variable, cycle)

    def read_var_at_cycle_1d(self, gname=None, variable=None, cycle=None,
            ip=None, jp=None, kp=None):
        """
        Read a variable along a particular direction at a particular time

        If the data is 1D, then ip, jp, kp are ignored. Otherwise, the output
        is made for fixed, not None, values of ip, jp, or kp. For 2D data one
        of the indices, associated with the dimension suppressed in the output
        is ignored, of the other two only one should be None. For 3D data only
        1 of ip, jp, and kp can be None.

        For example, for 3D output. If ip is None, and jp, and kp are ints,
        then this method returns:

            xi[i]  = coords[0][i]
            var[i] = variable[i, j=jp, k=kp]

        Returns a tuple (xi, var):
        * xi  : coordinate of the output
        * var : variable at xi
        """
        outid = self.__select_output_id__(gname, variable)
        return self.id2group[outid].read_var_at_cycle_1d(
                variable, cycle, ip, jp, kp)

    def get_group(self, gname=None):
        """
        Get a given group
        """
        if gname is None:
            return self.groups[0]
        else:
            return self.id2group[self.gname2id[gname]]

    def get_time(self, cycle):
        """
        Get the time corresponding to a specific cycle
        """
        outid = -1
        for group in self.groups:
            if group.times.has_key(cycle):
                return group.times[cycle]
        raise ValueError("Cycle not found in output: \"{}\"".format(cycle))
    def get_cycles(self, gname=None):
        """
        Get a list of cycles for a given variable group
        """
        if gname is None:
            outid = self.groups.keys()[0]
        else:
            outid = self.gname2id[gname]
        return self.groups[outid].cycles

    def __select_output_id__(self, gname=None, variable=None):
        if gname is None:
            if variable is None:
                outid = self.groups.keys()[0]
            else:
                outid = -1
                for group in self.groups:
                    if variable in group.variables:
                        outid = group.outid
                if outid == -1:
                    raise ValueError("Could not find the given dataset: "
                            "gname={} variable={}".format(gname, variable))
        else:
            outid = self.gname2id[gname]
        return outid

