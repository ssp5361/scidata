import numpy
import re
import os

import scidata.plain
import scidata.utils

def parse(liter):
    """
    Reads a timeserie from a file generated by the Multipole thorn,
    the data is returned as a tuple (t,z) where
    * t : numpy.array of times
    * z : complex numpy.array with the expansion coefficients

    liter should be an iteratable object returning datalines

    NOTE: The dataset class provides a more user-friendly way of
    accessing the same data
    """
    rawdata = scidata.utils.sorted_array(scidata.plain.parse(liter))
    t = rawdata[:,0]
    z = rawdata[:,1] + 1j*rawdata[:,2]
    return t, z

def readfiles(flist):
    """
    Reads a timeserie from a list of files generated by the Multipole thorn,
    the data is returned as a tuple (t,z) where
    * t : numpy.array of times
    * z : complex numpy.arrays with the expansion coefficients

    NOTE: The flist should contain a list of files refering to the same
    variable/multipoles/extraction radius.
    NOTE: The dataset class provides a more user-friendly way of
    accessing the same data
    """
    if type(flist) == str:
        flist = [flist]

    L = []
    for f in flist:
        L += open(f, 'r').readlines()

    return parse(L)

class dataset:
    """
    A class representing the output of the Multipole thorn in a simulation

    We store a dictionary { key : [files] } to be able to track the location
    of the data across the multiple files

    * data : dict of files

    * variables : list of variables
    * modes     : list of available (l, m) multipoles
    * lmodes    : list of available l multipoles
    * mmodes    : list of available m multipoles
    * radii     : list of available radii
    """

    def __init__(self, basedir='.', ignore_negative_m=False):
        """
        Initialize the dataset

        * basedir : data location
        """
        self.data = {}

        self.variables = set([])
        self.modes     = set([])
        self.radii     = set([])

        mp_re = r'mp_(\w+)_l(\d+)_m(\d+)_r(\d+\.\d\d).asc'

        filenames = scidata.utils.locate("mp_*.asc", basedir)
        for fname in filenames:
            mp_name = re.match(mp_re, os.path.basename(fname))
            if mp_name is not None:
                var = mp_name.group(1)
                l   = int(mp_name.group(2))
                m   = int(mp_name.group(3))
                if ignore_negative_m and m < 0:
                    continue
                r   = float(mp_name.group(4))

                self.variables.add(var)
                self.modes.add((l,m))
                self.radii.add(r)

                key = "%s_l%d_m%d_r%.2f" % (var, l, m, r)

                if self.data.has_key(key):
                    self.data[key].append(fname)
                else:
                    self.data[key] = [fname]

        self.variables = sorted(list(self.variables))
        self.modes = sorted(list(self.modes))
        self.lmodes = sorted(list(set([m[0] for m in self.modes])))
        self.mmodes = sorted(list(set([m[1] for m in self.modes])))
        self.radii = sorted(list(self.radii))

    def get(self, var=None, l=None, m=None, r=None):
        """
        Get the Multiple output for the given variable/multipole at the given
        extraction radius

        * var : if not specified it defaults to the first variable
        * l   : if not specified it defaults to 2 (or the minimum l
                if 2 is not available)
        * m   : if not specified it defaults to 2 (or the minimum m
                if 2 is not available)
        * r   : if not specified it defaults to the maximum radius
        """
        if var is None:
            var = self.variables[0]
        if l is None:
            if 2 in self.lmodes:
                l = 2
            else:
                l = self.modes[0][0]
        if m is None:
            if 2 in self.mmodes:
                m = 2
            else:
                m = self.modes[0][1]
        if r is None:
            r = self.radii[-1]

        key = "%s_l%d_m%d_r%.2f" % (var, l, m, r)

        return readfiles(self.data[key])
