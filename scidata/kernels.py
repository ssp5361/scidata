# This is partly taken from the SciPy Cookbook

import numpy

def gaussian(dim, size):
    """
    A multidimensional gaussian kernel

    * dim  : dimension
    * size : characteristic size of the kernel
    """
    sl = []
    for d in range(dim):
        sl.append(slice(-size, size+1))
    sl = tuple(sl)

    mgrid = numpy.lib.index_tricks.nd_grid()
    msh = mgrid[sl]

    expnt = numpy.zeros(2*size+1)
    for d in range(dim):
        expnt = expnt - msh[d]**2 / float(size)

    g = numpy.exp(expnt)
    return g / g.sum()

def square(dim, size):
    """
    A multidimensional square-window kernel

    * dim  : dimension
    * size : characteristic size of the kernel
    """
    shape = []
    for d in range(dim):
        shape.append(size)
    s = numpy.ones(shape)
    return s / s.sum()
