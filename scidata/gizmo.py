import h5py
import numpy
import re

class dataset:
    """
    A class representing the full output of a GIZMO simulation

    We store a dictionary { iter : file } to keep track of all of the files
    in the output

    * curr_it    : current iteration
    * dfile      : currently open datafile
    * index      : dictionary of files
    * iterations : list of iterations

    NOTE: the dataset is optimized for subsequential access to the data on a
    single iteration

    NOTE: this class is not thread safe
    """
    def __init__(self, filenames):
        """
        Initialize the dataset

        * filenames : can either be a list of filenames or a single filename
        """
        self.curr_it    = None
        self.dfile      = None
        self.index      = {}
        self.iterations = []

        if type(filenames) is str:
            filenames = [filenames]
        assert(len(filenames) > 0)

        for filename in filenames:
            fname_re = re.match(r".+\.(\d+)\.hdf5$", filename)
            if fname_re is not None:
                it = int(fname_re.group(1))
                self.index[it] = filename

        self.iterations = sorted(self.index.keys())

    def close_all_files(self):
        """
        Close all opened files
        """
        del self.dfile
        self.dfile   = None
        self.curr_it = None
    def set_iteration(self, it=None):
        """
        Set the current iteration

        * it : iteration number
        """
        if it is None:
            it = self.iterations[0]
        if it not in self.iterations:
            raise ValueError("Invalid iteration: " + str(it))
        if self.curr_it != it:
            self.dfile   = h5py.File(self.index[it], "r")
            self.curr_it = it

    def get_scalar_field_names(self, it=None, ptype="PartType0"):
        """
        Get a list of available scalar variable names

        * it : iteration number
        """
        self.set_iteration(it)
        fnames = []
        for dname in self.dfile[ptype].keys():
            if len(self.dfile[ptype][dname].shape) == 1:
                fnames.append(dname)
        return fnames
    def get_vector_field_names(self, it=None, ptype="PartType0"):
        """
        Get a list of available vector variable names

        * it : iteration number
        """
        self.set_iteration(it)
        vnames = []
        for dname in self.dfile[ptype].keys():
            if len(self.dfile[ptype][dname].shape) == 2:
                vnames.append(dname)
        return vnames

    def get_time(self, it=None):
        """
        Get the current time
        """
        self.set_iteration(it)
        return float(self.dfile["Header"].attrs["Time"])

    def get_scalar_field_data(self, it=None, name=None, dtype=numpy.float32,
            ptype="PartType0"):
        """
        Get a given field defined at the particle positions

        * it    : iteration number
        * name  : field name
        * dtype : datatype to use for the output
        * ptype : which particle type to read data from
        """
        if name is None:
            name = self.get_scalar_field_names(it, ptype)[0]
        self.set_iteration(it)
        return numpy.array(self.dfile[ptype][name], dtype=dtype)
    def get_vector_field_data(self, it=None, name=None, dtype=numpy.float32,
            ptype="PartType0", unpack=True):
        """
        Get a given vector field defined at the particle positions

        * it     : iteration number
        * name   : field name
        * dtype  : datatype to use for the output
        * ptype  : which particle type to read data from
        * unpack : unpacks arrays
        """
        if name is None:
            name = self.get_vector_field_names(it, ptype)[0]
        self.set_iteration(it)
        data = numpy.array(self.dfile[ptype][name], dtype=dtype)
        if unpack:
            return data[:,0], data[:,1], data[:,2]
        else:
            return data
    get_field_data = get_scalar_field_data

    def get_coordinates(self, it=None, dtype=numpy.float32, ptype="PartType0",
            unpack=True):
        """
        Get the coordinate locations of the particles

        * it    : iteration number
        * dtype : datatype to use for the output
        * ptype : which particle type to read data from
        """
        return self.get_vector_field_data(it=it, name="Coordinates",
                dtype=dtype, ptype=ptype, unpack=unpack)
