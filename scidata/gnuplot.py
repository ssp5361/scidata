import copy
import numpy
import re

def parse_1D_file(filename, cx=0, cy=1, index=0):
    """
    Parse a 1D ASCII file in gnuplot format

    * filename : the name of the file to parse
    * cx       : the column number of the x-data
    * cy       : the column number of the y-data
    * index    : the index to read

    On output this will return two numpy arrays x[i], y[i] with the parsed data
    """
    dfile = open(filename, 'r')

    x = []
    y = []

    idx = 0
    block = 0
    for dline in dfile:
        if re.match(r"\s+$", dline) is not None:
            block += 1
            if block == 2:
                block = 0
                idx += 1
        elif idx == index and dline[0] != '#':
            sdline = dline.split()
            x.append(float(sdline[cx]))
            y.append(float(sdline[cy]))

        if idx > index:
            break

    return (numpy.array(x), numpy.array(y))

def parse_2D_file(filename, cx=0, cy=1, cz=2, index=0):
    """
    Parse a 2D ASCII file in gnuplot format

    * filename : the name of the file to parse
    * cx       : the column number of the x-data
    * cy       : the column number of the y-data
    * cz       : the column number of the z-data
    * index    : the index to read

    On output this will return three numpy arrays x[i,j], y[i,j] and z[i,j]
    """
    dfile = open(filename, 'r')

    x = []
    y = []
    z = []

    xb = []
    yb = []
    zb = []

    idx = 0
    prev = False
    block = 0
    for dline in dfile:
        if re.match(r"\s+$", dline) is not None:
            block += 1
            if prev:
                idx += 1
            elif idx == index:
                x.append(copy.deepcopy(xb))
                y.append(copy.deepcopy(yb))
                z.append(copy.deepcopy(zb))
                xb = []
                yb = []
                zb = []
            prev = True
        elif idx == index and dline[0] != '#':
            prev = False
            sdline = dline.split()
            xb.append(float(sdline[cx]))
            yb.append(float(sdline[cy]))
            zb.append(float(sdline[cz]))

        if idx > index:
            break

    return (numpy.array(x), numpy.array(y), numpy.array(z))
