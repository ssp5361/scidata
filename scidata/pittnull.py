import numpy
import re
import os

import scidata.plain
import scidata.utils

def parse(liter, m=2):
    """
    Reads a timeseries from a waveform file generated by the PITTNull code,
    the data is returned as a tuple (t,z) where
    * t : numpy.array of times
    * z : complex numpy.array with the re-normalized Psi4

    * liter should be an iteratable object returning datalines
    * m is the azimuthal index of the spherical harmonic mode being read

    NOTE: The dataset class provides a more user-friendly way of
    accessing the same data
    """
    rawdata = scidata.utils.sorted_array(scidata.plain.parse(liter))
    t = rawdata[:,0]
    # Convert to the the same conventions as the Multipole thorn
    #   psi4^{NR} = (-1)^{m+1} 2 psi4^{CCE}
    sign = 1 if m % 2 != 0 else -1
    z = sign * 2 * (rawdata[:,1] + 1j*rawdata[:,2])
    return t, z

class dataset:
    """
    A class representing the waveform outputted by the PITTNull code

    * data   : list of files
    * modes  : list of available l modes
    * lmodes : list of available l multipoles
    * mmodes : list of available m multipoles
    """
    def __init__(self, basedir=".", ignore_negative_m=False):
        """
        Initialize the dataset

        * basedir : data location
        """
        self.data  = {}
        self.modes = []

        psi4_re = r'Psi4_scri.L(\d\d)M(p|m)(\d\d).asc'

        filenames = scidata.utils.locate("Psi4_scri.*.asc", basedir)
        for fname in filenames:
            psi4_match = re.match(psi4_re, os.path.basename(fname))
            if psi4_match is not None:
                l = int(psi4_match.group(1))
                s = psi4_match.group(2)
                if not (s == 'p' or s == 'm'):
                    continue
                m = (1 if s == 'p' else -1) * int(psi4_match.group(3))
                if ignore_negative_m and m < 0:
                    continue
                self.data[(l, m)] = fname
                self.modes.append((l,m))
        self.modes = sorted(self.modes)
        self.lmodes = sorted(list(set([m[0] for m in self.modes])))
        self.mmodes = sorted(list(set([m[1] for m in self.modes])))
    def get(self, l=None, m=None):
        """
        Get a given multipole of Psi4
        * l   : if not specified it defaults to 2 (or the minimum l
                if 2 is not available)
        * m   : if not specified it defaults to 2 (or the minimum m
                if 2 is not available)
        """
        if l is None:
            if 2 in self.lmodes:
                l = 2
            else:
                l = self.modes[0][0]
        if m is None:
            if 2 in self.mmodes:
                m = 2
            else:
                m = self.modes[0][1]
        return parse(open(self.data[(l,m)], "r"))
