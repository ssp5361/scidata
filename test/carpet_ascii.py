#!/usr/bin/env python

from scidata import monodataset
from scidata import carpet
from scidata.carpet import ascii
from timeit import timeit

liter = open("rho.x.asc", "r").readlines()

def test_old():
    data = carpet.ascii.parse_1D(liter, "x")
    assert data.nframes == 101

def test_new():
    data = carpet.ascii.parse_1D(liter, "x")
    assert data.nframes == 101

print timeit('test_old()', 'from __main__ import test_old', number=100)
print timeit('test_new()', 'from __main__ import test_new', number=100)
